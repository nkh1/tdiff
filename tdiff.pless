#!/bin/bash

DIFF_TOOL=vimdiff
FILE_EDIT=vim
DIR_EDIT=open_in_ftl

open_in_ftl() { tabs= ; for p in "$PWD/$1/$3" "$PWD/$2/$3" ; do [[ -e "$p" ]] && tabs+="$p$'\n'" ; done ; tmux new-window "ftl -t <(echo -n "$tabs")" ; }

bind tdiff mode		m		next_mode		"next view mode"
bind tdiff mode		M		previous_mode		"previous view mode"

bind tdiff find		n		next_difference		"next difference"
bind tdiff find		N		previous_difference	"previous difference"
bind tdiff find		gff		diff_fzf		"fzf select"
bind tdiff find		b		diff_fzf		"fzf select"

bind tdiff edit		ENTER		open_diff		"open diff"

bind tdiff synch	dd		delete			"delete both sides"
bind tdiff synch	dl		delete_left		"delete left"
bind tdiff synch	dr		delete_right		"delete rigt"
bind tdiff synch	tl		synch_to_left		"synchronize to left"
bind tdiff synch	tr		synch_to_right		"synchronize to right"

declare -A deleted

tdiff_setup()
{
mode=${e[mode]}

generate_index_keys

deleted_dir1=$(<"${e[only_d2]}" wc -l)
deleted_dir2=$(<"${e[only_d1]}" wc -l)
}

generate_index_keys()
{
index_file="${e[tree_${mode}_diff_index]}"

<"$index_file"         readarray -t diff_index
< <(tac "$index_file") readarray -t diff_index_reversed
}

tdiff_setup

previous_mode()
{
if   [[ "${e[mode]}" == all         ]] ; then e[mode]=all_d2_only
elif [[ "${e[mode]}" == common      ]] ; then e[mode]=all
elif [[ "${e[mode]}" == diff        ]] ; then e[mode]=common
else [[ "${e[mode]}" == all_d2_only ]] ; e[mode]=diff
fi

apply_mode
}

next_mode()
{
if   [[ "${e[mode]}" == all         ]] ; then e[mode]=common
elif [[ "${e[mode]}" == common      ]] ; then e[mode]=diff
elif [[ "${e[mode]}" == diff        ]] ; then e[mode]=all_d2_only
else [[ "${e[mode]}" == all_d2_only ]] ; e[mode]=all
fi

apply_mode
}

apply_mode()
{
[[ "${e[mode]}" == all         ]] && { tree1="${e[tree_all_d1]}"      ; tree2="${e[tree_all_d2]}"    ; }
[[ "${e[mode]}" == common      ]] && { tree1="${e[tree_common_d1]}"   ; tree2="${e[tree_common_d2]}" ; }
[[ "${e[mode]}" == diff        ]] && { tree1="${e[tree_diff]}"        ; tree2=                       ; }
[[ "${e[mode]}" == all_d2_only ]] && { tree1="${e[tree_all_d2_only]}" ; tree2=                       ; }

reload_input_streams $tree1 $tree2
generate_index_keys
}

open_diff()
{
no_color_key="tree_${e[mode]}_no_color"
entry="$(tail -n +$((top_line + 1)) "${e[$no_color_key]}" | head -n 1)"

entry="${entry#./}" ;
entry="${entry% <!>}" ;

if   [[ -f "${e[dir1]}/$entry" && -f "${e[dir2]}/$entry" ]] ; then $DIFF_TOOL "${e[dir1]}/$entry" "${e[dir2]}/$entry"
elif [[ -f "${e[dir1]}/$entry" ]] ;                           then $FILE_EDIT "${e[dir1]}/$entry"
elif [[ -f "${e[dir2]}/$entry" ]] ;                           then $FILE_EDIT "${e[dir2]}/$entry"
else                                                               $DIR_EDIT  "${e[dir1]}" "${e[dir2]}" "$entry"
fi

echo -en '\e[?1049h\e[2J' ; stty -echo ; tput civis

list
}

next_difference()
{
found=
for i in "${diff_index[@]}" ; do ((i > top_line)) && { found=$i ; break ; } ; done 
[[ "$found" ]] && { ((top_line = found)) ; highlight=1 ; list ; } || { highlight= ; list ; }
}

previous_difference()
{
for i in "${diff_index_reversed[@]}" ; do ((i < top_line)) && { found=$i ; break ; } ; done 
[[ "$found" ]] && { ((top_line = found)) ; highlight=1 ; list ; } || { highlight= ; list ; }
}

diff_fzf()
{
exec 2>&9

diff_file="$(cat ${e[diff_files]} | fzf +s --ansi --keep-right)"

no_color_key="tree_${e[mode]}_no_color"
found=$(rg -n -m 1 "\./$diff_file" "${e[$no_color_key]}" | cut -d: -f1)

[[ "$found" ]] && { ((top_line = found - 1)) ; highlight=1 ; } || { highlight= ; }

exec 2>"$fs/log"

echo -en '\e[?1049h' ; stty -echo ; tput civis

list
}

synch_to_left() { : ; }
synch_to_right() { : ; }

delete_left() { : ; }
delete_right() { : ; }

delete()
{
entry="$(tail -n +$((top_line + 1)) "${e[$no_color_key]}" | head -n 1)"

# if   [[ -f "${e[dir1]}/$entry" && -f "${e[dir2]}/$entry" ]] ; then lh=
# elif [[ -f "${e[dir1]}/$entry" ]] ;                           then lh=
# elif [[ -f "${e[dir2]}/$entry" ]] ;                           then lh=
# else                                                              
# fi

lh=

{ prompt "delete '$entry' [y|N] ? " -sn1 ; search_string="$REPLY" ; }
alt_screen

deleted[$top_line]=1

list
}

update_status()
{
no_color_key="tree_${e[mode]}_no_color"
current_entry="..$(tail -n +$((top_line + 1)) ${e[$no_color_key]} | head -n 1)"

status=$(cut -c1-$COLUMNS <<<"[${e[mode]}: -$deleted_dir1, -$deleted_dir2, !${#diff_index[@]}] $current_entry\e[m")
echo -en "\e[$((bottom_line + 2));1H\e[K\e[38;5;240m$status\e[m"
}

overlay_line()
{
(( ${deleted[$2]} )) &&
	{
	echo -en "\e[$1;0H${lines[$2]}\r\e[48;5;166;38;5;233m \e[m"
	return 0
	}

return 1
}

# vim: set ft=bash:


